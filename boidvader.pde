int currentTime;
int previousTime;
int deltaTime;
int compteur =50;

final float AccelBoulet = 5;  // how fast bullet flies
final int fireSpeed=150; // how often you fire / distance between bullets
int lastFired=millis();  // timer to determine when next bullet starts 


ArrayList<Mover> flock;
int flockSize = 50;
Spaceship ship;

boolean debug = false;

void setup () {
  fullScreen(P2D);
  currentTime = millis();
  previousTime = millis();
  
  flock = new ArrayList<Mover>();
  
  for (int i = 0; i < flockSize; i++) {
    Mover m = new Mover(new PVector(width/6, height/6), new PVector(random (-2, 2), random(-2, 2)));
    m.fillColor = color(random(255), random(255), random(255));
    flock.add(m);
  }
  ship = new Spaceship();

 // flock.get(0).debug = true;
}

void draw () {
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;
  
  

  
  update(deltaTime);
  display();  
}

/***
  The calculations should go here
*/
void update(int delta) {
  
   for(int la = 0 ; la < ship.laser.size(); la++)
 { 
   for (int i =0; i < flock.size(); i++)
   {
       if(flock.get(i).testCollising(ship.laser.get(la)))
       {
           flock.remove(i);
           ship.laser.get(la).isAlive = 0;
           compteur--;
           
       }
   }
    
 
   }
  
  for (int i =0; i < flock.size(); i++)
   {
       if(flock.get(i).testCollising(ship))
       {
         ship.isAlive = false;
       }
   }
  
  for (Mover m : flock) {
    m.flock(flock);
    m.update(delta);
  }
  ship.update();
}



/***
  The rendering should go here
*/
void display () {
  background(0);
  
  
  if(compteur ==0)
  {
    textSize(50);
    text("YOU WIN ! press R to restart", width/6, 100); 
  }
  else if(!(ship.isAlive))
  {
    textSize(50);
    text("YOU LOSE ! press R to restart", width/6, 100); 
  }
  else
  {
    textSize(25);
  text(compteur, width/2, 100); 
  
  }

for(Mover m : flock)
{
    m.display();
}
  
  ship.display();
  
  if (keyPressed) {
    if (key == 'q' || key == 'Q') {
      ship.turn(-0.03);
    } else if (key == 'd' ||key == 'D') {
      ship.turn(0.03);
    } else if (key == 'z' || key == 'Z') {
      ship.thrust(); 
    }
  }
 
}

void keyPressed() {
  
  switch (key) {
    case 'p':
      flock.get(0).debug = !flock.get(0).debug;
      break;
      
      case ' ':
          ship.fire();
      break;
      
     case 'r':
       setup();
     break;
  }
  
}
