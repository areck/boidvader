class Bullet extends GraphicObject {

  
  float w;
  int isAlive;
  float mass = 10;
  float life = 255;

  Bullet(float tempX, float tempY, // new2
  float tempSpeedX, float tempSpeedY, 
  float tempW) {
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector(0,0);
    
    isAlive = 1;
    
    location.x = tempX;
    location.y = tempY;
    w = tempW;
   
    velocity.x = tempSpeedX; // 4.2; // new2 
    velocity.y = tempSpeedY; // 0;
  }

  void move() {
    // Add speed to location
    location.x += velocity.x; 
    location.y += velocity.y;
   
   
  }
  
   void testCollision() {
    //Si la balle touche un mur, elle rebondit
    if (location.x > width-10 || location.x < 10) {
     /* velocity.x *= -0.9;
      velocity.y *= 0.9;
       if (location.x > width-20)
                location.x = width-20;
          else if (location.x < 20)
               location.x = 20;*/
               
               isAlive = 0;
    }
    if (location.y > height-10 || location.y < 10) {
     /* velocity.y *= -0.9;
      velocity.x *= 0.9;
          if (location.y > height-20)
                location.y = height-20;
          else if (location.y < 20)
                location.y = 20;*/
                isAlive = 0;
    }
  }
  boolean testCollising( Mover m)
 {
     if (PVector.dist(location,m.location) <= m.r + w)
     {
         return true;
         
     }
    else
    return false;
 }
  
  
  void applyForce (PVector force) {
    PVector f;
    
    if (mass != 1)
      f = PVector.div (force, mass);
    else
      f = force;
   
    acceleration.add(f);    
  }
  
  void update(float deltaTime)
  {
     // Velocity changes according to acceleration
    velocity.add(acceleration);
    // position changes by velocity
    location.add(velocity);
    // We must clear acceleration each frame
    acceleration.mult(0);
    
  }


  void display() {
    
    
    fill(244, 2, 2);
    noStroke();
 
   
   ellipse(location.x,location.y, w, w);
    
    
  }
}
